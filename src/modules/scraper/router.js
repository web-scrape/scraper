import { Router } from 'express'

const router = Router() 

router.get('/status', (req, res) => {  
  res.json({
    FAILED: true,
  })
})

export default router
