import 'colors'

import express from 'express'
import fs from 'fs'
import path from 'path'

import { MODULES_PATH } from '../config.json'

const app = express()

const MODULES_FULLPATH = path.join(__dirname, MODULES_PATH)
const modules = fs.readdirSync(MODULES_FULLPATH)

modules.forEach((module) => {
  const MODULE_PATH = path.join(MODULES_FULLPATH, module)

  const moduleFile = require(MODULE_PATH)

  moduleFile.default(app)

  console.log(` [#] MODULE CONNECTED => ${module}`.green)
})

app.listen(4000)
